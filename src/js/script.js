import {Input, Textarea, Select} from "./elements/index.js"
import {Modal, ModalAddVisit, ModalLogin} from "./classes/modal/index.js"
import {Form, LoginForm, SelectVisitForm, CardioVisitForm, TherapistVisitForm, DentistVisitForm} from "./classes/form/index.js";
import {Visit, VisitCardiologist, VisitDentist, VisitTherapist} from "./classes/visit/index.js";
import {requestAddCard, requestChangeCard, requestLogin, requestRemoveCard, requestShowAllCards} from "./functions/index.js";