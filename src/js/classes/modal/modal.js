export default class Modal {
    constructor(props) {
        this.props = {...props};
        this.modalInside = `<div class="modal-content">
                                   <span class="close">&times;</span>
                                   <h3 class="modal-header">Выберете нужное действие</h3>
                                   <div class="modal-btns">
                                        <input type="button" value="login" id="loginBtn" class="login-btn">
                                        <input type="button" value="addVisit" id="addVisitBtn" class="add-visit-btn">
                                   </div>            
                            </div>`;
        this.close = this.modalRemove.bind(this);
    }

    modalShow() {
        this.elem.classList.add("active");
    }

    modalRemove() {
        this.elem.classList.remove("active");
    }

    render() {
        const {className, id} = this.props;
        const modal = document.createElement("div");
        modal.className = className;
        modal.id = id;
        modal.innerHTML = this.modalInside;
        const close = modal.querySelector(".close");
        close.addEventListener("click", this.close);
        this.elem = modal;
        return modal;
    }
}
export const modalProps = {
    className: "modal",
    id: "modalMain",
};

const newModal = new Modal(modalProps);
document.body.prepend(newModal.render());
const addVisitor = document.getElementById('create-visitor');
addVisitor.addEventListener('click', newModal.modalShow.bind(newModal));

const loginVisitor = document.getElementById('loginBtn');
loginVisitor.addEventListener('click', newModal.modalRemove.bind(newModal));

const addCard = document.getElementById('addVisitBtn');
addCard.addEventListener('click', newModal.modalRemove.bind(newModal));

