import Input from "../../elements/input.js";
import Modal from "./modal.js";
export default class ModalLogin extends Modal {
  constructor(props) {
    super();
    this.props = {...props};
    this.modalInside = `<div class="login-modal-content" id="login-content">
                                   <span class="close">&times;</span>
                                   <h3 class="login-modal-header">Авторизация</h3>
                            </div>`
  }
  renderLogin(){
    const {className, id} = this.props;
    const login = document.createElement("div");
    login.className = className;
    login.id = id;
    login.innerHTML = this.modalInside;
    const close = login.querySelector(".close");
    close.addEventListener("click", this.close);
    this.elem = login;
    return login;
  }
}

const loginProps = {
  className: "login-modal",
  id: "loginMain",
};
const loginInput = new Input('text', 'login-modal-input', 'Your name', 'loginInput');
const passwordInput = new Input('password', 'login-modal-password', 'Your password', 'passwordInput');
const confirmInput = new Input('submit', 'login-modal-submit', 'Login', 'confirmInput')
const loginModal = new ModalLogin(loginProps);
document.body.prepend(loginModal.renderLogin());
const loginVisitor = document.getElementById('loginBtn');
loginVisitor.addEventListener('click', loginModal.modalShow.bind(loginModal));
const mainModal = document.getElementById('login-content');
mainModal.append(loginInput.render());
mainModal.append(passwordInput.render());
mainModal.append(confirmInput.render());

