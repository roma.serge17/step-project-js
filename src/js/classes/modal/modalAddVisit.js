import Modal from "./modal.js";
import Input from "../../elements/input.js";
import Select from "../../elements/select.js";
export default class ModalAddVisit extends Modal{
    constructor(props) {
        super();
        this.props = {...props};
        this.modalInside = `<div class="select-modal-content" id="visit-content">
                                   <span class="close">&times;</span>
                                   <h3 class="select-modal-header">Выберете врача</h3>
                            </div>`
    }
    renderVisit(){
        const {className, id} = this.props;
        const login = document.createElement("div");
        login.className = className;
        login.id = id;
        login.innerHTML = this.modalInside;
        const close = login.querySelector(".close");
        close.addEventListener("click", this.close);
        this.elem = login;
        return login;
    }

}
const visitProps = {
    className: "select-modal",
    id: "visitMain",
};
const select = new Select('select-modal-select', 'selectDoctor', 'dentist', 'cardiologist', 'therapist');
const input = new Input('button', 'select-modal-btn', 'Create', 'createVisit');
const newVisit = new ModalAddVisit(visitProps);
document.body.prepend(newVisit.renderVisit());
const addVisit = document.getElementById('addVisitBtn');
addVisit.addEventListener('click', newVisit.modalShow.bind(newVisit));
const addElements = document.getElementById('visit-content');
addElements.append(select.render());
addElements.append(input.render());
