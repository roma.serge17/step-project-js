export default class Select {
    constructor(className, name, doctorFirst, doctorSecond, doctorThird) {
        this.className = className;
        this.doctorFirst = doctorFirst;
        this.doctorSecond = doctorSecond;
        this.doctorThird = doctorThird;
        this.name = name;
    }
    render(){
        const select = document.createElement('select');
        select.className = this.className;
        select.name = name;
        select.insertAdjacentHTML('beforeend', `<option>${this.doctorFirst}</option>,<option>${this.doctorSecond}</option>,<option>${this.doctorThird}</option>,` )
        return select
    }
}