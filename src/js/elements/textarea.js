export default class Textarea {
    constructor(className, name, cols, rows) {
        this.className = className;
        this.name = name;
        this.cols = cols;
        this.rows = rows;
    }
    render(){
        const textarea = document.createElement('textarea');
        textarea.className = this.className;
        textarea.name = this.name;
        textarea.cols = this.cols;
        textarea.rows = this.rows;
        return textarea
    }
}