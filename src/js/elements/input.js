export default class Input {
    constructor(type, className, placeholder, name) {
        this.type = type;
        this.className = className;
        this.placeholder = placeholder;
        this.name = name;
    }
    render(){
        const input = document.createElement('input');
        input.type = this.type;
        input.className = this.className;
        input.placeholder = this.placeholder;
        input.name = this.name;
        return input
    }
}